import React, { useReducer, createContext } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { initialCardState, reducerCard } from './Reducers/CardReducer';

import Home from './Home/Home';

export const CardContext = createContext(null);

const App = () => {
	const [card, dispatchCard] = useReducer(reducerCard, initialCardState);

	return (
		<CardContext.Provider value={{ card, dispatchCard }}>
			<Router>
				<Switch>
					<Route exact path="/">
						<Home />
					</Route>
				</Switch>
			</Router>
		</CardContext.Provider>
	);
};

export default App;
