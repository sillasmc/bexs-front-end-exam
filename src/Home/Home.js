import React from 'react';

import Header from './Header/Header';
import AddCard from './AddCard/AddCard';

import './Home.scss';

const Home = () => {
	return (
		<div className="home">
			<Header />
			<AddCard />
		</div>
	);
};

export default Home;
