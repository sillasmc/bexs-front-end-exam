import React from 'react';

import Box from './Box/Box';

import './Header.scss';

const Header = () => {
	return (
		<div className="header">
			<div className="header__slogan"></div>
			<div className="header__boxes">
				<Box />
				<Box />
				<Box />
				<Box />
				<Box />
			</div>
		</div>
	);
};

export default Header;
