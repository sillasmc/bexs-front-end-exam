import React, { useContext } from 'react';
import classNames from 'classnames';

import { CardContext } from '../../../App';

import './Card.scss';

const Card = () => {
	const { card } = useContext(CardContext);

	const cardStyle = classNames({
		card: true,
		'card--back': card.showCardBack,
		'card--visa': card.cardNumber[0] === '4',
		'card--visa-back': card.showCardBack && card.cardNumber[0] === '4',
	});

	const cardLogoStyle = classNames({
		card__logo: !card.showCardBack && card.cardNumber[0] === '4',
	});

	const getCardNumber = () => {
		if (card.cardNumber) {
			return card.cardNumber.replace(/_/g, '*');
		} else {
			return '**** **** **** ****';
		}
	};

	const getFrontContent = () => (
		<>
			<div className={cardLogoStyle}></div>
			<p className="card__text">{getCardNumber()}</p>
			<span className="card__text card__text--left">
				{card.name || 'NOME DO TITULAR'}
			</span>
			<span className="card__text card__text--right">
				{card.expirationDate || '00/00'}
			</span>
		</>
	);

	const getBackContent = () => <span className="card-back__text">***</span>;

	return (
		<div className={cardStyle}>
			{card.showCardBack ? getBackContent() : getFrontContent()}
		</div>
	);
};

export default Card;
