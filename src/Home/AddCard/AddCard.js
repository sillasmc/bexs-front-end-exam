import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

import Card from './Card/Card';
import AddCardForm from './AddCardForm/AddCardForm';

import './AddCard.scss';

const AddCard = () => {
	const goBack = () => alert('Escolher nova forma de pagamento');

	return (
		<div className="add-card">
			<div className="add-card__red-panel">
				<div className="go-back" onClick={goBack}>
					<span className="go-back__arrow">
						<FontAwesomeIcon icon={faAngleLeft} />
					</span>
					<span className="go-back__text">
						Alterar forma de pagamento
					</span>
				</div>
				<div className="steps">Etapa 2 de 3</div>
				<div className="new-card">
					<div className="new-card__logo"></div>
					<span className="new-card__text">
						Adicionar um novo cartão de crédito
					</span>
				</div>
			</div>
			<Card />
			<AddCardForm value={12000} />
		</div>
	);
};

export default AddCard;
