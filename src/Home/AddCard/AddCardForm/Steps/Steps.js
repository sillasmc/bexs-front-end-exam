import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';

import Step from './Step/Step';

import './Steps.scss';

const Steps = () => {
	return (
		<div className="form-steps">
			<Step step={1} name="Carrinho" checked />
			<FontAwesomeIcon icon={faAngleRight} />
			<Step step={2} name="Pagamento" />
			<FontAwesomeIcon icon={faAngleRight} />
			<Step step={3} name="Confirmação" />
		</div>
	);
};

export default Steps;
