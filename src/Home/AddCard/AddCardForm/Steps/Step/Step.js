import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import './Step.scss';

const Step = ({ step, name, checked = false }) => {
	return (
		<span className="step">
			{checked && <FontAwesomeIcon icon={faCheckCircle} />}
			{!checked && <span className="step__number">{step}</span>}
			<span className="step__name">{name}</span>
		</span>
	);
};

Step.propTypes = {
	step: PropTypes.number,
	name: PropTypes.string,
	checked: PropTypes.bool,
};

export default Step;
