import React, { useState, useContext } from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import MaskedInput from 'react-text-mask';

import { CardContext } from '../../../../App';
import { Types } from '../../../../Reducers/Actions';

import './CustomInput.scss';

const CustomInput = ({
	id,
	label,
	type,
	mask,
	labelIcon,
	field,
	children,
	touched,
	errors,
}) => {
	const { dispatchCard } = useContext(CardContext);

	const [hovered, setHovered] = useState(false);
	const [focus, setFocus] = useState(false);
	const labelStyle = classNames({
		'custom-input__label': true,
		'custom-input__label--hover': hovered || focus || field.value,
	});
	const containerStyle = classNames({
		'custom-input': true,
		'custom-input--with-error': touched[field.name] && errors[field.name],
	});

	const handleBlur = () => {
		setFocus(false);
		if (field.onBlur) {
			field.onBlur();
		}
	};

	const handleFocus = () => {
		setFocus(true);
		if (field.onFocus) {
			field.onFocus();
		}
	};

	const handleChange = (evt) => {
		let type = '';
		switch (field.name) {
			case 'cardNumber':
				type = Types.ADD_CARD_NUMBER;
				break;
			case 'name':
				type = Types.ADD_NAME;
				break;
			case 'expirationDate':
				type = Types.ADD_EXPIRATION_DATE;
				break;
			case 'code':
				type = Types.ADD_CODE;
				break;
			case 'parcels':
				type = Types.ADD_PARCELS;
				break;
			default:
				break;
		}
		let actionContent = {
			type,
		};
		actionContent[field.name] = evt.target.value;
		dispatchCard(actionContent);
		field.onChange(evt);
	};

	const getSelect = () => (
		<>
			<select
				id={id}
				className="custom-input__input custom-input__input--select"
				onFocus={handleFocus}
				onBlur={handleBlur}
				{...field}
				onChange={handleChange}
			>
				{children}
			</select>
			<FontAwesomeIcon
				className="custom-input__icon"
				icon={faAngleDown}
			/>
		</>
	);

	const getMaskedInput = () => (
		<MaskedInput
			id={id}
			mask={mask}
			className="custom-input__input"
			type={type}
			onFocus={handleFocus}
			onBlur={handleBlur}
			{...field}
			onChange={handleChange}
		/>
	);

	const getStandardInput = () => (
		<input
			id={id}
			className="custom-input__input"
			type={type}
			onFocus={handleFocus}
			onBlur={handleBlur}
			{...field}
			onChange={handleChange}
		/>
	);

	const getInputType = () => {
		if (type === 'select' && children) {
			return getSelect();
		} else if (mask) {
			return getMaskedInput();
		} else {
			return getStandardInput();
		}
	};

	return (
		<div
			className={containerStyle}
			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
			onClick={() => setFocus(true)}
		>
			<label className={labelStyle} htmlFor={id}>
				{`${label}  `}
				{labelIcon}
			</label>
			{getInputType()}
			{touched[field.name] && errors[field.name] && (
				<div className="custom-input__error">{errors[field.name]}</div>
			)}
		</div>
	);
};

export default CustomInput;
