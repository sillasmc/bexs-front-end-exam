import React, { useContext } from 'react';
import { Formik, Field, Form } from 'formik';
import { Container, Row, Col } from 'react-grid-system';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

import Steps from './Steps/Steps';
import CustomInput from './CustomInput/CustomInput';
import { Parcels } from '../../../Constants/Parcels';
import { Masks, ValidationSchema } from '../../../Constants/Form';
import { formatCurrency } from '../../../Utils/Currency';
import {
	validateName,
	validateCode,
	validateCardNumber,
	validateDate,
} from '../../../Utils/Validation';
import { CardContext } from '../../../App';
import { Types } from '../../../Reducers/Actions';
import { payment } from '../../../Api/Card';

import './AddCardForm.scss';

const AddCardForm = ({ value }) => {
	const { dispatchCard } = useContext(CardContext);

	const getParcels = () =>
		Parcels.map((parcel) => (
			<option key={parcel} value={parcel}>{`${parcel}x ${formatCurrency(
				value / parcel
			)} sem juros`}</option>
		));

	return (
		<div className="add-form">
			<Steps />
			<Formik
				initialValues={{
					cardNumber: '',
					name: '',
					expirationDate: '',
					code: '',
					parcels: '',
				}}
				validationSchema={ValidationSchema}
				onSubmit={(values, { setSubmitting }) => {
					setTimeout(() => {
						alert(JSON.stringify(values, null, 2));
						payment(values);
						setSubmitting(false);
					}, 400);
				}}
			>
				<Form className="add-form__form">
					<Container>
						<Row>
							<Field
								name="cardNumber"
								validate={validateCardNumber}
								render={({
									field,
									form: { touched, errors },
								}) => (
									<CustomInput
										field={field}
										touched={touched}
										mask={Masks.cardNumber}
										id="cardNumber"
										label="Número do cartão"
										errors={errors}
									/>
								)}
							/>
						</Row>
						<Row>
							<Field
								name="name"
								validate={validateName}
								render={({
									field,
									form: { touched, errors },
								}) => (
									<CustomInput
										field={field}
										touched={touched}
										id="name"
										label="Nome (igual ao cartão)"
										type="text"
										errors={errors}
									/>
								)}
							/>
						</Row>
						<Row>
							<Col className="add-form__form-col" xs={6}>
								<Field
									name="expirationDate"
									validate={validateDate}
									render={({
										field,
										form: { touched, errors },
									}) => (
										<CustomInput
											field={field}
											mask={Masks.expirationDate}
											touched={touched}
											id="expirationDate"
											label="Validade"
											type="text"
											errors={errors}
										/>
									)}
								/>
							</Col>
							<Col className="add-form__form-col" xs={6}>
								<Field
									name="code"
									validate={validateCode}
									render={({
										field,
										form: { touched, errors },
									}) => (
										<CustomInput
											field={field}
											touched={touched}
											id="code"
											label="CVV"
											mask={Masks.code}
											labelIcon={
												<FontAwesomeIcon
													onClick={() =>
														dispatchCard({
															type:
																Types.TOGGLE_SHOW_CARD_BACK,
														})
													}
													icon={faInfoCircle}
												/>
											}
											type="text"
											errors={errors}
										/>
									)}
								/>
							</Col>
						</Row>
						<Row>
							<Field
								name="parcels"
								render={({
									field,
									form: { touched, errors },
								}) => (
									<CustomInput
										field={field}
										touched={touched}
										id="parcels"
										label="Parcelas"
										type="select"
										errors={errors}
									>
										<option key={0}></option>
										{getParcels()}
									</CustomInput>
								)}
							></Field>
						</Row>
						<Row justify="end">
							<button className="add-form__button" type="submit">
								CONTINUAR
							</button>
						</Row>
					</Container>
				</Form>
			</Formik>
		</div>
	);
};

export default AddCardForm;
