import * as Yup from 'yup';

export const Masks = {
	expirationDate: [/\d/, /\d/, '/', /\d/, /\d/],
	code: [/\d/, /\d/, /\d/],
	cardNumber: [
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		' ',
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		' ',
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		' ',
		/\d/,
		/\d/,
		/\d/,
		/\d/,
	],
};

export const ValidationSchema = Yup.object({
	cardNumber: Yup.string().required('Obrigatório'),
	name: Yup.string().required('Obrigatório'),
	expirationDate: Yup.string().required('Obrigatório'),
	code: Yup.string().required('Obrigatório'),
	parcels: Yup.string().required('Insira o número de parcelas'),
});
