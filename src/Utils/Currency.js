export const formatCurrency = (
	value,
	locale = 'pt-BR',
	style = 'currency',
	currency = 'BRL',
	minimumFractionDigits = 2
) => {
	const formatter = new Intl.NumberFormat(locale, {
		style,
		currency,
		minimumFractionDigits,
	});

	return formatter.format(value);
};
