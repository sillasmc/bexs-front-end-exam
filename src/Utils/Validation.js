export const validateName = (value) => {
	let error;
	const splitted = value.split(' ');
	if (splitted.length === 1 || splitted[1] === '') {
		error = 'Insira seu nome completo';
	}
	return error;
};

export const validateCode = (value) => {
	let error;
	const numbers = value.replace(/_/g, '');
	if (numbers.length < 3) {
		error = 'Código inválido';
	}
	return error;
};

export const validateCardNumber = (value) => {
	let error;
	const numbers = value.replace(/\D/g, '');
	if (numbers.length < 16) {
		error = 'Número de cartão inválido';
	}
	return error;
};

export const validateDate = (value) => {
	let error;
	const numbers = value.replace(/\D/g, '');
	if (numbers.length < 4) {
		error = 'Data inválida';
	} else {
		const splitted = value.split('/');
		const today = new Date();
		let yearBeggin = `${today.getFullYear()}`.slice(0, 2);
		const date = new Date(`${yearBeggin}${splitted[1]}-${splitted[0]}-15`);
		if (!(date instanceof Date && !isNaN(date))) {
			error = 'Data inválida';
		} else if (
			date.getFullYear() < today.getFullYear() ||
			(date.getFullYear() === today.getFullYear() &&
				date.getMonth() <= today.getMonth())
		) {
			error = 'Data inválida';
		}
	}
	return error;
};
