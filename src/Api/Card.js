import axios from './configure';

export const payment = (cardForm) => {
	return axios.post('/pagar', {
		card: cardForm,
	});
};
