import { Types } from './Actions';

export const initialCardState = {
	cardNumber: '',
	name: '',
	expirationDate: '',
	code: '',
	parcels: '',
	showCardBack: false,
};

export const reducerCard = (state, action) => {
	switch (action.type) {
		case Types.ADD_CARD_NUMBER:
			return { ...state, cardNumber: action.cardNumber };
		case Types.ADD_NAME:
			return { ...state, name: action.name };
		case Types.ADD_EXPIRATION_DATE:
			return { ...state, expirationDate: action.expirationDate };
		case Types.ADD_CODE:
			return { ...state, code: action.code };
		case Types.ADD_PARCELS:
			return { ...state, parcels: action.parcels };
		case Types.TOGGLE_SHOW_CARD_BACK:
			return { ...state, showCardBack: !state.showCardBack };
		default:
			return state;
	}
};
