import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('Test App', () => {
	beforeAll(() => {
		render(<App />);
	});

	it('Should render app title', () => {
		expect(screen.queryByText('Boilerplate')).not.toBeNull();
	});
});
