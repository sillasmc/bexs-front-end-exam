const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const argv = require('yargs').argv;

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'build'),
		publicPath: '/',
		filename:
			argv.mode === 'production'
				? '[name].[contenthash].js'
				: '[name].js',
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all',
				},
			},
		},
	},
	devtool: 'inline-source-map',
	devServer: {
		historyApiFallback: true,
		port: 3000,
		open: true,
		hot: true,
		noInfo: false,
		quiet: false,
		host: '0.0.0.0',
		public: 'localhost:3000',
		stats: 'errors-only',
		contentBase: './build',
	},
	resolve: {
		modules: ['node_modules'],
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ['babel-loader', 'eslint-loader'],
			},
			{
				test: /\.(scss|css|sass)$/,
				use: ['style-loader', 'css-loader', 'sass-loader'],
			},
			{
				test: /\.(png|jpe?g|gif)$/i,
				use: [
					{
						loader: 'file-loader',
					},
				],
			},
			{
				test: /\.svg$/,
				use: [
					{
						loader: 'svg-url-loader',
						options: {
							limit: 10000,
						},
					},
				],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Caching',
			template: path.resolve('./public/index.html'),
			favicon: path.resolve('./public/favicon.ico'),
		}),
		new Dotenv(),
	],
};
